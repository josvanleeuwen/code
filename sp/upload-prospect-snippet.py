@user_passes_test(is_employee, login_url=reverse_lazy('login_employee'))
@permission_required('company.change_prospect', raise_exception=True)
def company_upload_prospect(request):
    errors = {}
    employee = request.user.get_profile().employee
    partner = employee.person.partner

    def handle_upload(file):
        # To avoid importing existing clients as prospect a set is created with
        # all existing subdomain labels of TLDs, taken from e-mail addresses
        # E.g. 'info@stageplaza.nl' returns 'stageplaza' which will be checked
        # when importing new prospects for duplicates.
        user_emails = User.objects.all().values_list('email', flat=True)
        prospect_emails = Prospect.objects.exclude(email__isnull=True, email='').values_list('email', flat=True)

        emails = list(user_emails) + list(prospect_emails)
        domains = set()

        for email in emails:
            try:
                domains.add(re.findall('@(.+)\..+$', email)[0])
            except IndexError:
                pass  # Invalid e-mail address

        # Start saving prospects while generating the CSV. Any output in
        # retval will be yielded in a table while streaming a response.
        dr = csv.DictReader(file, delimiter=';')
        yield u'''
            <html>
                <head>
                    <title>Prospects importeren</title>
                </head>
                <body>
                    <table border='1'>
                        <thead>
                            <tr>
                                <th style='width: 10%;'>Regel</th>
                                <th>Melding</th>
                            </tr>
            '''

        for linenr, row_data in enumerate(dr, start=1):
            retval = ""
            try:
                # Check validity of e-mail address
                email = row_data.get('email').lower().decode('utf-8')[:255]

                try:
                    validate_email(email)
                except ValidationError:
                    retval += u"Skipped: invalid e-mail address: {0}<br />".format(email)
                    continue

                # Check domain field for duplicate prospects
                try:
                    domain = re.findall('@(.+)\..+$', email)[0]
                except IndexError:
                    retval += u"Skipped: couldn't get a correct domain from e-mail: {0}<br />".format(email)
                    continue

                if domain in domains:
                    retval += u"Skipped: prospect domain exists: {0}<br />".format(domain)
                    continue

                # Create prospect and hope it's unique!
                p = Prospect(status=Prospect.STATUS_IMPORTED,
                             added_by=employee,
                             added_via=Prospect.ADDED_VIA_CSV)

                p.email = email
                p.company_name = row_data.get('company_name').decode('utf-8')[:155]
                p.website = row_data.get('website', '').decode('utf-8')[:200]
                p.label = row_data.get('label', '').decode('utf-8')[:250]
                p.mailvars = row_data.get('mailvars', '').decode('utf-8')

                # Create the person and user for the prospect
                # If errors happen either retrieve existing user/person
                # Or set the user/person value to None in the prospect
                try:
                    person = Person(status=Person.STATUS_INACTIVE, partner=partner)
                    person.job_title = row_data.get('job_title', '').decode('utf-8')[:50]
                    gender = row_data.get('gender').decode('utf-8')[:4]
                    person.gender = Person.GENDER_MALE if gender == 'm' else (Person.GENDER_FEMALE if gender == 'v' else 2)
                    person.telephone_number = re.sub('[^0-9]', '', re.sub('\+', '00', row_data.get('telephone', '')))[:20]
                    try:
                        user = User()
                        user.email = email
                        user.username = email
                        user.first_name = row_data.get('first_name', '').decode('utf-8')[:30]
                        user.last_name = row_data.get('last_name', '').decode('utf-8')[:30]
                        user.save()
                    except IntegrityError, e:
                        retval += "User: {0}<br />".format(e)
                        transaction.commit_unless_managed()
                        user = User.objects.get(email=email)
                        user_exists = True
                        try:
                            p.company = user.person.main_company.all()[0]
                            p.status = Prospect.STATUS_CONVERTED
                        except IndexError:
                            p.company = None
                    except ValueError, err:
                        retval += u"User: {0}<br />".format(unicode(err))
                        user = None
                    person.user = user
                    person.save()
                except (IntegrityError, AttributeError), e:
                    retval += "User: {0}<br />".format(e)
                    transaction.commit_unless_managed()
                    person = Person.objects.get(user=user)
                except ValueError, err:
                    retval += u"Person: {0}<br />".format(unicode(err))

                    if user and not user_exists:
                        user.delete()
                    person = None

                # Add the company newsletter mailtype to the person
                if person:
                    person.mail_types.add(MailType.objects.get(model_identifier=MailType.IDENTIFIER_COMPANY, mail_type=MailType.MAIL_TYPE_NEWSLETTER))

                try:
                    address = Address(address_type=Address.ADDRESS_TYPE_CONTACT)
                    address.street = row_data.get('street').decode('utf-8')[:250]
                    address.house_number = row_data.get('house_number').decode('utf-8')[:20]
                    address.zip_code = row_data.get('zip_code').decode('utf-8')[:10]
                    address.city = row_data.get('city').decode('utf-8')[:150]

                    try:
                        address.country = Country.objects.get(id=row_data.get('country'))
                    except Country.DoesNotExist:
                        # 155 = Nederland
                        address.country = Country.objects.get(id=155)

                    address.save()
                except (ValueError, IntegrityError), err:
                    retval += u"Address: {0}<br />".format(unicode(err))
                    transaction.commit_unless_managed()
                    address = None

                p.person = person
                p.address = address
                p.save()

                retval += u"Imported prospect {0}: {1}".format(linenr, p.company_name)
            except (ValueError, IntegrityError), err:
                retval += u"All: {0}<br />".format(unicode(err))
                transaction.commit_unless_managed()
                if p and p.id:
                    p.delete()
                if user and user.id:
                    user.delete()
                if person and person.id:
                    person.delete()
                if address and address.id:
                    address.delete()
            finally:
                try:
                    del p
                    del person
                    del address
                except:
                    pass  # Enige waarop dit fout kan gaan is als je hierboven ook in de except terecht kwam.

                yield u"<tr><td>{0}</td><td>{1}</td></tr>\n".format(linenr, retval)

        yield u"</table></body></html>"

    # -------------------------------------------------------------------------

    if request.method == 'POST':
        form = forms.ProspectUploadForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            file = form.cleaned_data['csv']
            return StreamingHttpResponse(handle_upload(file))

    else:
        form = forms.ProspectUploadForm()

    return render_to_response('company/manage/prospects_upload.html',
                              {'errors': errors,
                               'form': form,
                               'selected': 'prospects-companies'},
                              context_instance=RequestContext(request))
