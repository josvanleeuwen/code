from django.contrib import admin

from about.models import Profile, ProfileAvatarVideo, SocialProfile


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'job_title', 'status')
    list_filter = ('status', )
    prepopulated_fields = {"slug": ("first_name", "last_name")}
    search_fields = ['first_name', 'last_name']


class ProfileAvatarVideoAdmin(admin.ModelAdmin):
    pass


class SocialProfileAdmin(admin.ModelAdmin):
    pass


admin.site.register(Profile, ProfileAdmin)
admin.site.register(ProfileAvatarVideo, ProfileAvatarVideoAdmin)
admin.site.register(SocialProfile, SocialProfileAdmin)
