from django.db import models
from django.template.defaultfilters import slugify

from utils.storage import OverwriteStorage

import markdown
import os
import sorl

class Profile(models.Model):
    def avatar_path(instance, filename):
        filename, ext = os.path.splitext(filename)
        filename = 'avatar' + ext
        return os.path.join('about/{first_name}-{last_name}'.format(first_name=slugify(instance.first_name), last_name=slugify(instance.last_name)), filename)

    def cover_path(instance, filename):
        filename, ext = os.path.splitext(filename)
        filename = 'cover' + ext
        return os.path.join('about/{first_name}-{last_name}'.format(first_name=slugify(instance.first_name), last_name=slugify(instance.last_name)), filename)

    def background_path(instance, filename):
        filename, ext = os.path.splitext(filename)
        filename = 'background' + ext
        return os.path.join('about/{first_name}-{last_name}'.format(first_name=slugify(instance.first_name), last_name=slugify(instance.last_name)), filename)

    STATUS_OFFLINE = 0
    STATUS_PRIVATE = 1
    STATUS_ONLINE = 2

    __STATUS_CHOICES = ((STATUS_PRIVATE, u'prive'),
                        (STATUS_ONLINE, u'online'),
                        (STATUS_OFFLINE, u'offline'), )

    first_name = models.CharField(u'voornaam', max_length=100, blank=True)
    last_name = models.CharField(u'achternaam', max_length=100, blank=True)
    slug = models.SlugField('slug', max_length=200)
    job_title = models.CharField(u'functie', max_length=100, blank=True)
    avatar = models.ImageField(u'avatar', storage=OverwriteStorage(), upload_to=avatar_path, blank=True, help_text=u'Afbeelding verhouding 4:3')
    cover = models.ImageField(u'omslagfoto', storage=OverwriteStorage(), upload_to=cover_path, blank=True, help_text=u'Afbeelding wordt automatisch naar 1600:300 gesneden.')
    background = models.ImageField(u'achtergrond', storage=OverwriteStorage(), upload_to=background_path, blank=True, help_text=u'Afbeelding verhouding: 16:9 (960x540)')
    slogan = models.CharField(u'slogan', max_length=200)
    biography = models.TextField(u'biografie', null=True, blank=True)
    biography_html = models.TextField(u'html biografie', null=True, blank=True, editable=False)
    status = models.PositiveIntegerField(u'status', choices=__STATUS_CHOICES)
    priority = models.PositiveIntegerField(u'prioriteit', help_text=u'Rangschikking op prioriteit, 1 staat bovenaan.')

    class Meta:
        ordering = ['first_name', 'last_name', 'status']
        verbose_name = "profiel"
        verbose_name_plural = "profielen"

    def save(self, *args, **kwargs):
        if self.avatar:
            sorl.thumbnail.delete(self.avatar, delete_file=False)
        if self.cover:
            sorl.thumbnail.delete(self.cover, delete_file=False)
        if self.background:
            sorl.thumbnail.delete(self.background, delete_file=False)

        self.biography_html = markdown.markdown(self.biography, extensions=['extra', 'nl2br', 'sane_lists'], output_format='html5')
        super(Profile, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'{0} {1} ({2})'.format(self.first_name, self.last_name, self.job_title)


class ProfileAvatarVideo(models.Model):
    FORMAT_MP4 = 0
    FORMAT_WEBM = 1
    FORMAT_OGG = 2

    __FORMAT_CHOICES = ((FORMAT_OGG, u'ogg'),
                        (FORMAT_MP4, u'mp4'),
                        (FORMAT_WEBM, u'webm'), )

    def avatar_path(instance, filename):
        filename, ext = os.path.splitext(filename)
        filename = 'avatar-video' + ext
        return os.path.join('about/{first_name}-{last_name}'.format(first_name=slugify(instance.profile.first_name), last_name=slugify(instance.profile.last_name)), filename)

    video = models.FileField(u'avatar video', storage=OverwriteStorage(), upload_to=avatar_path, help_text=u'Video verhouding 4:3')
    format = models.PositiveIntegerField(u'formaat', choices=__FORMAT_CHOICES)
    profile = models.ForeignKey('Profile', verbose_name='profiel', related_name='avatarvideos')

    class Meta:
        ordering = ['format', 'profile__first_name', 'profile__last_name']
        verbose_name = "profiel avatarvideo"
        verbose_name_plural = "profiel avatarvideos"

    def __unicode__(self):
        return u'{0} ({1} {2}) '.format(self.get_format_display(), self.profile.first_name, self.profile.last_name)


class SocialProfile(models.Model):
    MEDIUM_EMAIL = 0
    MEDIUM_FACEBOOK = 1
    MEDIUM_GOOGLEPLUS = 2
    MEDIUM_TWITTER = 3
    MEDIUM_INSTAGRAM = 4
    MEDIUM_LINKEDIN = 5
    MEDIUM_VIMEO = 6
    MEDIUM_YOUTUBE = 7
    MEDIUM_WEBSITE = 8

    __MEDIUM_CHOICES = ((MEDIUM_EMAIL, u'E-Mail'),
                        (MEDIUM_FACEBOOK, u'Facebook'),
                        (MEDIUM_GOOGLEPLUS, u'Google+'),
                        (MEDIUM_TWITTER, u'Twitter'),
                        (MEDIUM_INSTAGRAM, u'Instagram'),
                        (MEDIUM_LINKEDIN, u'LinkedIn'),
                        (MEDIUM_VIMEO, u'Vimeo'),
                        (MEDIUM_YOUTUBE, u'YouTube'),
                        (MEDIUM_WEBSITE, u'Website'), )

    profile = models.ForeignKey('Profile', verbose_name='profiel', related_name='social')
    medium = models.PositiveIntegerField(u'medium', choices=__MEDIUM_CHOICES)
    url = models.CharField('url of e-mail', max_length=200)

    class Meta:
        ordering = ['profile__first_name', 'profile__last_name', 'medium']
        verbose_name = "social media profiel"
        verbose_name_plural = "social media profielen"

    def __unicode__(self):
        return u'{0} {1} ({2})'.format(self.profile.first_name, self.profile.last_name, self.get_medium_display())
