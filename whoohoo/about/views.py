from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext

from about.models import Profile


def about(request):
    profiles = Profile.objects.all().order_by('priority')
    return render_to_response('about/about.html',
                              {'selectnav': 'about',
                               'profiles': profiles},
                              RequestContext(request))


def profile(request, profile_slug):
    profile = get_object_or_404(Profile, slug=profile_slug)
    return render_to_response('about/profile.html',
                              {'selectnav': 'about',
                               'profile': profile},
                              RequestContext(request))
