from django.conf.urls import patterns, url


urlpatterns = patterns('about.views',
    url(r'^$', 'about', name='about'),
    url(r'^(?P<profile_slug>[\w-]+)/$', 'profile', name='about_profile'),
)
