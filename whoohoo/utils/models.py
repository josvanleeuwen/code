# Encoding: utf-8
from django.db import models
from datetime import datetime


class ContactMail(models.Model):
    name = models.CharField('naam', max_length=100)
    email = models.EmailField('email', max_length=100)
    timestamp_sent = models.DateTimeField('verzendatum', editable=False, default=datetime.now())
    message = models.TextField('bericht')

    class Meta:
        verbose_name = 'contactmail'
        verbose_name_plural = 'contactmails'
        ordering = ['name']

    def __unicode__(self):
        return u'{0} ({1})'.format(self.name, self.timestamp_sent)


class LinkPartnerCategory(models.Model):
    name = models.CharField('naam', max_length=100)

    class Meta:
        verbose_name = u'linkpartner categorie'
        verbose_name_plural = u'linkpartner categoriën'
        ordering = ['name']

    def __unicode__(self):
        return self.name


class LinkPartner(models.Model):
    name = models.CharField('naam', max_length=100)
    url = models.URLField('linkadres')
    desc = models.TextField(u'omschrijving', null=True, blank=True)
    category = models.ForeignKey('LinkPartnerCategory', verbose_name='categorie')

    class Meta:
        verbose_name = 'linkpartner'
        verbose_name_plural = 'linkpartners'
        ordering = ['name']

    def __unicode__(self):
        return u'{0} ({1})'.format(self.name, self.url)
