from django.contrib import admin

from utils.models import ContactMail, LinkPartnerCategory, LinkPartner


admin.site.register(ContactMail)
admin.site.register(LinkPartnerCategory)
admin.site.register(LinkPartner)
