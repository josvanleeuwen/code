from django.shortcuts import render_to_response
from django.template import RequestContext
from dance.models import Dance


def form_error_handler(form):
    """handles the field errors for the given form"""
    if form.errors:
        for f_name in form.fields:
            if f_name in form.errors:
                classes = form.fields[f_name].widget.attrs.get('class', '')
                classes += ' error'
                form.fields[f_name].widget.attrs['class'] = classes


def sitemap(request):
    dances = Dance.objects.all()

    return render_to_response('utils/sitemap.xml',
                              {'dances': dances, },
                              context_instance=RequestContext(request),
                              content_type="application/xml")
