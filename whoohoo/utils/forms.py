from django.forms import ModelForm

from utils.models import ContactMail
from utils.views import form_error_handler as error_handler


class ContactForm(ModelForm):
    class Meta:
        model = ContactMail
        fields = ['name', 'email', 'message']

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        error_handler(self)
