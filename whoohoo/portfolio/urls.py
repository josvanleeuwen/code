from django.conf.urls import patterns, url

urlpatterns = patterns('portfolio.views',
    url(r'^$', 'portfolio', name='portfolio'),
    url(r'^tag/(?P<tag_slug>[\w-]+)/$', 'portfolio', name='portfolio_tag'),
    url(r'^(?P<client_slug>[\w-]+)/$', 'client', name='portfolio_client'),
    url(r'^(?P<client_slug>[\w-]+)/(?P<project_slug>[\w-]+)/$', 'project', name='portfolio_project'),
    # url(r'^whoohoo/', include('whoohoo.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
