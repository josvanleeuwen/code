from django.contrib import admin

from portfolio.models import Client, Project, Still, Tag


class ClientAdmin(admin.ModelAdmin):
    list_display = ('name', 'status')
    list_filter = ('status', )
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ['name']


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('title', 'date', 'client_name', 'status')
    list_filter = ('tags', 'client__name', 'status')
    prepopulated_fields = {"slug": ("title",)}
    search_fields = ['title']


class StillAdmin(admin.ModelAdmin):
    list_display = ('title', 'project_title', 'image')
    list_filter = ('project__title', 'project__client__name')
    search_fields = ['title']


class TagAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


admin.site.register(Client, ClientAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Still, StillAdmin)
admin.site.register(Tag, TagAdmin)
