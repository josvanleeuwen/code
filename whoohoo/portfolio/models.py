from django.db import models

from utils.storage import OverwriteStorage

import os
import markdown
import sorl


class Client(models.Model):
    def file_path(instance, filename):
        filename, ext = os.path.splitext(filename)
        filename = 'logo' + ext
        return os.path.join('portfolio/{client}'.format(client=instance.slug), filename)

    STATUS_OFFLINE = 0
    STATUS_PRIVATE = 1
    STATUS_ONLINE = 2

    __STATUS_CHOICES = ((STATUS_PRIVATE, u'prive'),
                        (STATUS_ONLINE, u'online'),
                        (STATUS_OFFLINE, u'offline'), )

    name = models.CharField(u'klantnaam', max_length=250, unique=True)
    slug = models.SlugField(u'slug', max_length=100, unique=True)
    logo = models.ImageField(u'logo', storage=OverwriteStorage(), upload_to=file_path, blank=True)
    url = models.URLField(u'URL', null=True, blank=True)
    desc = models.TextField(u'omschrijving', null=True, blank=True)
    desc_html = models.TextField(u'html omschrijving', null=True, blank=True, editable=False)
    status = models.PositiveIntegerField(u'status', choices=__STATUS_CHOICES)

    class Meta:
        ordering = ['name', 'status']
        verbose_name = "klant"
        verbose_name_plural = "klanten"

    def save(self, *args, **kwargs):
        if self.logo:
            sorl.thumbnail.delete(self.logo, delete_file=False)

        self.desc_html = markdown.markdown(self.desc, extensions=['extra', 'nl2br', 'sane_lists'], output_format='html5')
        super(Client, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'{0}'.format(self.name)


class Project(models.Model):
    STATUS_OFFLINE = 0
    STATUS_PRIVATE = 1
    STATUS_ONLINE = 2

    __STATUS_CHOICES = ((STATUS_PRIVATE, u'prive'),
                        (STATUS_ONLINE, u'online'),
                        (STATUS_OFFLINE, u'offline'), )

    def thumbnail_path(instance, filename):
        filename, ext = os.path.splitext(filename)
        filename = 'thumbnail' + ext
        return os.path.join('portfolio/{client}/{project}'.format(client=instance.client.slug, project=instance.slug), filename)

    def background_path(instance, filename):
        filename, ext = os.path.splitext(filename)
        filename = 'background' + ext
        return os.path.join('portfolio/{client}/{project}'.format(client=instance.client.slug, project=instance.slug), filename)

    client = models.ForeignKey('Client', verbose_name='klant')
    title = models.CharField(u'projecttitel', max_length=100, unique=True)
    slug = models.SlugField(u'slug', max_length=100, unique=True)
    date = models.DateField(u'datum')
    thumbnail = models.ImageField(u'thumbnail', storage=OverwriteStorage(), upload_to=thumbnail_path, blank=True, help_text=u'Afbeelding verhouding 16:9')
    background = models.ImageField(u'achtergrond', storage=OverwriteStorage(), upload_to=background_path, blank=True, help_text=u'Afbeelding verhouding 16:9')
    desc = models.TextField(u'omschrijving', null=True, blank=True)
    desc_html = models.TextField(u'html omschrijving', null=True, blank=True, editable=False)
    vimeo = models.CharField(u'vimeo id', max_length=20, null=True, blank=True)
    url = models.URLField(u'URL', null=True, blank=True)
    status = models.PositiveIntegerField(u'status', choices=__STATUS_CHOICES)
    tags = models.ManyToManyField('Tag', verbose_name='label')

    class Meta:
        verbose_name = 'project'
        verbose_name_plural = 'projecten'
        ordering = ['-date', 'title']

    def client_name(self):
        return self.client.name
    client_name.short_description = 'klantnaam'

    def save(self, *args, **kwargs):
        if self.thumbnail:
            sorl.thumbnail.delete(self.thumbnail, delete_file=False)
        if self.background:
            sorl.thumbnail.delete(self.background, delete_file=False)

        self.desc_html = markdown.markdown(self.desc, extensions=['extra', 'nl2br', 'sane_lists'], output_format='html5')
        super(Project, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'{0}: {1} ({2})'.format(self.client.name, self.title, self.get_status_display())


class Still(models.Model):
    def file_path(instance, filename):
        return os.path.join('portfolio/{client}/{project}'.format(client=instance.project.client.slug, project=instance.project.slug), filename)

    title = models.CharField(u'titel', max_length=250)
    project = models.ForeignKey('Project', verbose_name='project')
    image = models.ImageField(u'afbeelding', storage=OverwriteStorage(), upload_to=file_path, blank=True)

    class Meta:
        verbose_name = 'still'
        verbose_name_plural = 'stills'
        ordering = ['title']

    def project_title(self):
        return self.project.title
    project_title.short_description = 'projecttitel'

    def save(self, *args, **kwargs):
        if self.image:
            sorl.thumbnail.delete(self.image, delete_file=False)

        super(Still, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'{0}: {1} ({2})'.format(self.project.title, self.title, self.image.name)


class Tag(models.Model):
    name = models.CharField(u'label', max_length=100)
    slug = models.SlugField(u'slug', max_length=100, unique=True)
    priority = models.PositiveIntegerField(u'prioriteit', help_text=u'Rangschikking op prioriteit, 1 staat bovenaan.')

    class Meta:
        verbose_name = 'label'
        verbose_name_plural = 'labels'
        ordering = ['name']

    def __unicode__(self):
        return self.name

'''
class RootBlock(models.Model):
    SIZE_1 = 1
    SIZE_2 = 2
    SIZE_3 = 3

    __SIZE_CHOICES = ((SIZE_1, u'250 pixels'),
                      (SIZE_2, u'500 pixels'),
                      (SIZE_3, u'750 pixels'), )

    width = models.PositiveIntegerField(u'breedte', choices=__SIZE_CHOICES)
    position = models.PositiveIntegerField(u'positie')

class Block(models.Model):
    SIZE_1 = 1
    SIZE_2 = 2
    SIZE_3 = 3

    __SIZE_CHOICES = ((SIZE_1, u'250 pixels'),
                      (SIZE_2, u'500 pixels'),
                      (SIZE_3, u'750 pixels'), )

    width = models.PositiveIntegerField(u'breedte', choices=__SIZE_CHOICES)
    height = models.PositiveIntegerField(u'hoogte', choices=__SIZE_CHOICES)
    project = models.ForeignKey('Project', verbose_name='project')
    position = models.PositiveIntegerField(u'positie')
'''
