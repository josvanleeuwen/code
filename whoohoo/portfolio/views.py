from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext

from portfolio.models import Client, Project, Tag


def portfolio(request, tag_slug=None):
    tags = Tag.objects.all().order_by('priority')

    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        portfolio = Project.objects.filter(status=Project.STATUS_ONLINE, tags=tag).order_by('-date')
        tag_selected = tag.slug
    else:
        portfolio = Project.objects.filter(status=Project.STATUS_ONLINE).order_by('-date')
        tag_selected = None

    return render_to_response('portfolio/portfolio.html',
                              {'selectnav': 'portfolio',
                               'tags': tags,
                               'tag_selected': tag_selected,
                               'portfolio': portfolio, }, RequestContext(request))


def client(request, client_slug):
    client = get_object_or_404(Client, slug=client_slug, status=Client.STATUS_ONLINE)

    return render_to_response('portfolio/client.html',
                              {'selectnav': 'portfolio',
                               'client': client, }, RequestContext(request))


def project(request, client_slug, project_slug):
    portfolio = Project.objects.filter(status=Project.STATUS_ONLINE)
    project = get_object_or_404(Project, client__slug=client_slug, slug=project_slug, status__in=[Project.STATUS_ONLINE, Project.STATUS_PRIVATE])

    return render_to_response('portfolio/project.html',
                              {'selectnav': 'portfolio',
                               'portfolio': portfolio,
                               'project': project, }, RequestContext(request))
