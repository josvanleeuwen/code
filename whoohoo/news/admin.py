from django.contrib import admin

from news.models import Article


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'status')
    list_filter = ('status', )
    prepopulated_fields = {"slug": ("title",)}
    search_fields = ['title', 'content']


admin.site.register(Article, ArticleAdmin)
