from django.db import models

import markdown


class Article(models.Model):
    STATUS_DRAFT = 1
    STATUS_PUBLISHED = 2

    __STATUS_CHOICES = ((STATUS_DRAFT, u'ontwerp'),
                        (STATUS_PUBLISHED, u'gepubliceerd'), )

    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200)
    content = models.TextField(null=True, blank=True)
    content_html = models.TextField(editable=False, null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey('about.Profile', null=True, blank=True)
    status = models.PositiveIntegerField(u'status', choices=__STATUS_CHOICES)

    class Meta:
        ordering = ['-date']
        verbose_name = "artikel"
        verbose_name_plural = "artikelen"

    def save(self, *args, **kwargs):
        self.content_html = markdown.markdown(self.content, extensions=['extra', 'nl2br', 'sane_lists'], output_format='html5')
        super(Article, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'{0}'.format(self.title)
