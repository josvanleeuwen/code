$( document ).ready(function() {
    scrolldistance = 500;

    htmlblocks = $( "[id^='rootblock']" );
    blocks = [];
    blocks_width = 0;
    for ( var i = 0; i < htmlblocks.length; i++ ) {
        blocks.push( htmlblocks.eq(i) );
        blocks[i].css("left", blocks_width);
        blocks_width += blocks[i].width();
    }
    // If necessary fill the screen up with cloned blocks
    i = 0;
    idcounter = blocks.length + 1;
    max = blocks.length - 1;
    while( blocks_width < window.screen ) {
        blocks.push( blocks[i].clone().attr("id","rootblock-" + idcounter ).css("left", blocks_width).appendTo( "#window" ) );
        blocks_width += blocks[i].width();
        idcounter++;
        if ( i < max ) {
            i++;
        } else {
            i = 0;
        }
    }

    $( "#prev" ).click(function() {
        // create and remove blocks
        $( "[id^='rootblock']" ).animate({ "left": "+=" + scrolldistance + "px", "easing": "easeInOutCirc"}, "slow" );
    });
    $( "#next" ).click(function() {
        // create and remove blocks
        $( "[id^='rootblock']" ).animate({ "left": "-=" + scrolldistance + "px", "easing": "easeInOutCirc"}, "slow" );
    });
});
