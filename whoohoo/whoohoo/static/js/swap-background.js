function swap_background(bg) {
    var $bgimg = new Image();
    $bgimg.src = bg;
    $bgimg.onload = function(){
        $("#background #media").append('<div style="background: url(' + bg + ') no-repeat center center; opacity: 0; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;" data-large="' + bg + '"></div>');
        $('#background #media div:last').animate({ opacity: 1 }, 1000, function(){
            if($('#background #media div').length > 1) {
                $('#background #media div')[0].remove();
            }
        });
    }
}
