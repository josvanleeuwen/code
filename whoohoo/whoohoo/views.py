from django.shortcuts import render_to_response
from django.template import RequestContext
from portfolio.models import Client, Project
from news.models import Article


def home(request):
    news = Article.objects.filter(status=Article.STATUS_PUBLISHED).order_by('-date')[:3]
    projects = Project.objects.filter(status=Project.STATUS_ONLINE).order_by('-date')[:1]

    return render_to_response('index.html',
                              {'selectnav': 'home',
                               'news': news,
                               'projects': projects},
                              RequestContext(request))


def reel(request):
    clients = Client.objects.filter(status=Client.STATUS_ONLINE).order_by('name').values_list('name', flat=True)
    portfolio = Project.objects.filter(status=Project.STATUS_ONLINE)

    return render_to_response('reel.html',
                              {'selectnav': 'reel',
                               'clients': clients,
                               'portfolio': portfolio, },
                              RequestContext(request))


def contact(request):
    return render_to_response('contact.html',
                              {'selectnav': 'contact'},
                              RequestContext(request))


def terms(request):
    return render_to_response('terms.html',
                              RequestContext(request))
