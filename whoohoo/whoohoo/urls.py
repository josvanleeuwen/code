from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'whoohoo.views.home', name='home'),
    url(r'^contact/$', 'whoohoo.views.contact', name='contact'),
    url(r'^reel/$', 'whoohoo.views.reel', name='reel'),
    url(r'^about/', include('about.urls')),
    url(r'^portfolio/', include('portfolio.urls')),
    url(r'^terms-and-conditions/$', 'whoohoo.views.terms', name='terms'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))
